# CHANGELOG

## v0.2.2

* Removed contents of pyproject.toml
* Updated readme

## v0.2.1

* Fixed a bug in the previous release
* Changed back from `setup.cfg` to `setup.py` because it makes the long description look weird

## v0.2.0

* Added `pyproject.toml`, should work properly now

## v0.1.4

* Updated README and Pypi description

## v0.1.3

* Added `minMatchSize` and `matchCharacter` optional arguments
* Added this readme
* Updated README and Pypi description

## v0.1.2

* Updated README and added Pypi description

## v0.1.1

* Min Python version is now 3
* Added README

## v0.1.0

* Initial project
