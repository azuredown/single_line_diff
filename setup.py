
from setuptools import setup

setup(
	name='single_line_diff',
	version='0.2.2',
	description='A package that can diff single lines',
	url='https://gitlab.com/azuredown/single_line_diff',
	author='Andrew Zuo',
	author_email='andrew@andrewzuo.com',
	python_requires='>=3',
	long_description=open("README.md").read(),
)
